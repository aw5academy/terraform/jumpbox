resource "aws_security_group" "jumpbox" {
  description = "jumpbox security group"
  name        = "jumpbox"
  tags = {
    Name = "jumpbox"
  }
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "jumpbox-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.jumpbox.id
  to_port           = 0
  type              = "egress"
}
