#!/bin/bash

while getopts "d" opt; do
  case "$opt" in
    d ) DESKTOP="true" ;;
  esac
done

getJumpBoxInstanceId() {
  JUMPBOX_INSTANCE_ID=(`aws ec2 describe-instances \
                          --query 'Reservations[*].Instances[].{ID:InstanceId}' \
                          --output text \
                          --filters "Name=instance-state-name,Values=running" \
                                    "Name=tag:Name,Values=jumpbox" \
                          --region $AWS_REGION`)
  if [ ! $JUMPBOX_INSTANCE_ID ]; then
    printf "No jumpbox instance found!\n"
    exit 1
  fi
}

getJumpboxKmsKeyId() {
  JUMPBOX_KMS_KEY_ID="$(aws kms describe-key --key-id alias/jumpbox \
                                             --region $AWS_REGION \
                                             |jq -r .KeyMetadata.KeyId)"
  if [ ! $JUMPBOX_KMS_KEY_ID ]; then
    printf "No jumpbox KMS key found!\n"
    exit 1
  fi
}

generateRandomUsernameAndPassword() {
  USERNAME="`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 6 | head -n 1`"
  PASSWORD="`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`"
  HASHED_PASSWORD="$(echo $PASSWORD | openssl passwd -6 -stdin)"
}

putHashedPasswordToParameterStore(){
  aws ssm put-parameter --name /jumpbox/users/${USERNAME} \
                        --value $HASHED_PASSWORD \
                        --type SecureString \
                        --key-id $JUMPBOX_KMS_KEY_ID \
                        --region $AWS_REGION > /dev/null
  if [ "$?" != "0" ]; then
    printf "Error putting hashed password to parameter store parameter: /jumpbox/users/${USERNAME}! Aborting!\n"
    exit 1
  fi
}

sendSsmCommandToCreateTempUser() {
  aws ssm send-command --document-name "jumpbox-create-temp-user" \
	               --parameters username="${USERNAME}" \
		       --instance-ids $JUMPBOX_INSTANCE_ID \
		       --document-version '$LATEST' \
		       --region $AWS_REGION > /dev/null
}

printConnectionDetails() {
  printf "Connect via Remote Desktop:\n"
  printf "Computer:  localhost:55678\n"
  printf "User name: ${USERNAME}\n"
  printf "Password:  ${PASSWORD}\n"
}

startRdpPortForwardingSsmSession() {
  aws ssm start-session --target $JUMPBOX_INSTANCE_ID \
	                --region $AWS_REGION \
			--document-name AWS-StartPortForwardingSession \
			--parameters "localPortNumber=55678,portNumber=3389"
}

startSsmSession() {
  aws ssm start-session --target $JUMPBOX_INSTANCE_ID \
	                --region $AWS_REGION
}

getJumpBoxInstanceId
if [ "$DESKTOP" == "true" ]; then
  getJumpboxKmsKeyId
  generateRandomUsernameAndPassword
  putHashedPasswordToParameterStore
  sendSsmCommandToCreateTempUser
  printConnectionDetails
  startRdpPortForwardingSsmSession
else
  startSsmSession
fi
