{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ssm:GetParameter",
      "Resource": [
        "arn:aws:ssm:us-east-1:${aws_account_id}:parameter/jumpbox/users/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "ssm:DeleteParameter",
      "Resource": [
        "arn:aws:ssm:us-east-1:${aws_account_id}:parameter/jumpbox/users/*"
      ]
    },
    {
      "Action":[
        "kms:CreateGrant",
        "kms:Decrypt",
        "kms:DescribeKey"
      ],
      "Resource":[
        "arn:aws:kms:us-east-1:${aws_account_id}:key/${kms_key_id}"
      ],
      "Effect":"Allow"
    },
    {
      "Action":[
        "kms:CreateGrant",
        "kms:ListGrants",
        "kms:RevokeGrant"
      ],
      "Resource":"*",
      "Effect":"Allow",
      "Condition":{
        "Bool":{
          "kms:GrantIsForAWSResource":"true"
        }
      }
    }
  ]
}
