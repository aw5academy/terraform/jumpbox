#!/bin/bash

CHEF_URL="https://packages.chef.io/files/stable/chef-workstation/20.6.62/el/8/chef-workstation-20.6.62-1.el7.x86_64.rpm"

downloadAndInstallChef() {
  if which chef > /dev/null; then
    return 0
  fi
  wget "$CHEF_URL" -O /tmp/chef.rpm
  rpm -i /tmp/chef.rpm
  rm -f /tmp/chef.rpm
}

installGit() {
  yum install -y git
}

berksPackage() {
  local tempdir=`mktemp -d`
  pushd $tempdir > /dev/null
  git clone https://gitlab.com/aw5academy/chef/jumpbox.git .
  export HOME=/root
  berks package /tmp/cookbooks.tar.gz
  popd > /dev/null
  rm -rf $tempdir
}

chefConverge() {
  if [ -d /etc/chef/bootstrap ]; then
    rm -rf /etc/chef/bootstrap
  fi
  mkdir -p /etc/chef/bootstrap
  pushd /etc/chef/bootstrap > /dev/null
  tar zxf /tmp/cookbooks.tar.gz
  rm -f /tmp/cookbooks.tar.gz
  cp -r cookbooks/jumpbox/environments .
  chef-client --chef-license accept -z -E build -o jumpbox
  popd > /dev/null
}

downloadAndInstallChef
installGit
berksPackage
chefConverge
