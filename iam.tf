data "aws_iam_policy_document" "ec2" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "jumpbox" {
  assume_role_policy = data.aws_iam_policy_document.ec2.json
  name               = "jumpbox"
  tags = {
    Name = "jumpbox"
  }
}

resource "aws_iam_instance_profile" "jumpbox" {
  name  = "jumpbox"
  role  = aws_iam_role.jumpbox.name
}

data "template_file" "jumpbox-policy" {
  template = file("${path.module}/templates/jumpbox-ec2-policy.json.tpl")
  vars = {
    aws_account_id = data.aws_caller_identity.current.account_id
    kms_key_id     = aws_kms_key.jumpbox.key_id
  }
}

resource "aws_iam_role_policy" "jumpbox" {
  name   = "jumpbox"
  policy = data.template_file.jumpbox-policy.rendered
  role   = aws_iam_role.jumpbox.id
}

data "aws_iam_policy" "session-manager" {
  arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/session-manager"
}

resource "aws_iam_role_policy_attachment" "session-manager" {
  policy_arn = data.aws_iam_policy.session-manager.arn
  role       = aws_iam_role.jumpbox.id
}
