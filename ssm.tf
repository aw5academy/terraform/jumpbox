resource "aws_ssm_document" "jumpbox-create-temp-user" {
  name          = "jumpbox-create-temp-user"
  document_type = "Command"
  document_format = "YAML"
  target_type     = "/AWS::EC2::Instance"
  tags = {
    Name    = "jumpbox-create-temp-user"
  }
  content = <<DOC
---
schemaVersion: '2.2'
description: Create a temporary user on the jumpbox
parameters:
  username:
    type: String
    description: "The username of the user to create."
mainSteps:
- action: aws:runShellScript
  name: runShellScript
  inputs:
    timeoutSeconds: '60'
    runCommand:
    - "bash -x /root/create-temp-user.sh '{{ username }}'"
DOC
}
