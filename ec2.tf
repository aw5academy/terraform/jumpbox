data "aws_ami" "amazon-linux-2" {
 most_recent = true
 filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
 owners      = ["amazon"]
}

data "template_file" "user-data" {
  template = file("${path.module}/templates/user-data.tpl")
}

resource "aws_instance" "jumpbox" {
  ami                    = data.aws_ami.amazon-linux-2.id
  iam_instance_profile   = aws_iam_instance_profile.jumpbox.name
  instance_type          = "t3a.micro"
  subnet_id              = var.private_subnet_id
  tags = {
    Name = "jumpbox"
  }
  user_data              = base64encode(data.template_file.user-data.rendered)
  vpc_security_group_ids = [aws_security_group.jumpbox.id]
}
