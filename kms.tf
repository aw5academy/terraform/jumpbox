data "template_file" "jumpbox-cmk" {
  template = "${file("${path.module}/templates/jumpbox-cmk-policy.json.tpl")}"
  vars = {
    aws_account_id = data.aws_caller_identity.current.account_id
  }
}

resource "aws_kms_key" "jumpbox" {
  deletion_window_in_days = "7"
  description             = "The encryption key for jumpbox."
  enable_key_rotation     = "true"
  policy                  = data.template_file.jumpbox-cmk.rendered
  tags = {
    Name = "jumpbox"
  }
}

resource "aws_kms_alias" "jumpbox" {
  name          = "alias/jumpbox"
  target_key_id = aws_kms_key.jumpbox.key_id
}
