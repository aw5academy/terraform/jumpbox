variable "private_subnet_id" {
  description = "The id of the private subnet where the resources will be created."
}

variable "vpc_id" {
  description = "The id of the VPC where the resources will be created."
}
