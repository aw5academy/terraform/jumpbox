# jumpbox
Creates an Amazon Linux 2 Jumpbox with a Desktop environment.

# Prerequisites
[AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html) must be configured to deploy this solution. You may deploy the Terraform code at [aw5academy/terraform/session-manager](https://gitlab.com/aw5academy/terraform/session-manager) to do this. See the instructions in the [README.md](https://gitlab.com/aw5academy/terraform/session-manager/-/blob/master/README.md).

When the `session-manager` stack is deployed, set some Terraform variables to be used with this `jumpbox` stack:
```
export TF_VAR_private_subnet_id=`terraform output private-subnet-id`
export TF_VAR_vpc_id=`terraform output vpc-id`
```

# Usage
* Deploy the stack with:
  ```
  git clone https://gitlab.com/aw5academy/terraform/jumpbox.git
  cd jumpbox
  terraform init
  terraform apply
  ```

# Cleanup
Cleanup this stack by running the following commands:
```
terraform init
terraform destroy
```

Remember to also perform the cleanup of the [aw5academy/terraform/session-manage](https://gitlab.com/aw5academy/terraform/session-manager) stack you created as a prerequisite. Follow the [README.md](https://gitlab.com/aw5academy/terraform/session-manager/-/blob/master/README.md) instructions to do this.
